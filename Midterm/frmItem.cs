﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class frmItem : Form
    {

        private Item item = null;

        public frmItem()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;
            decimal price = 0;
            try
            {
                price = Convert.ToDecimal(txtPrice.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid price value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int quantity = Convert.ToInt32(numQuantity.Value);
            int limit = Convert.ToInt32(numLimit.Value);
            if (this.Tag != null)
            {
                item = (Item)this.Tag;
                item.Name = name;
                item.Price = price;
                item.Quantity = quantity;
                item.Limit = limit;
            }
            else
            {
                item = new Item(name, price, quantity, limit);
            }
            this.Tag = item;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmItem_Load(object sender, EventArgs e)
        {
            item = (Item)this.Tag;
            if (item != null)
            {
                txtCode.Text = item.Code.ToString();
                txtName.Text = item.Name;
                txtPrice.Text = String.Format("{0}", item.Price);
                numQuantity.Value = item.Quantity;
                numLimit.Value = item.Limit;
            }
        }
    }
}
