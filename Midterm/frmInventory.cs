﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class frmInventory : Form
    {

        private Inventory inventory = new Inventory();

        public frmInventory()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmItem form = new frmItem();
            form.Tag = null;
            DialogResult res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                Item item = (Item)form.Tag;
                inventory.Add(item);
                uiUpdateInventory();
            }
        }

        private void uiAddToInventory(Item item)
        {
            if (!chkReorderOnly.Checked || (chkReorderOnly.Checked && item.Quantity < item.Limit))
            {
                ListViewItem view = new ListViewItem();
                view.SubItems.Add(item.Code.ToString());
                view.SubItems.Add(item.Name);
                view.SubItems.Add(String.Format("{0:C2}", item.Price));
                view.SubItems.Add(item.Quantity.ToString());
                view.SubItems.Add(item.Limit.ToString());
                view.Tag = item;
                lstInventory.Items.Add(view);
            }
        }

        public void uiUpdateInventory()
        {
            lstInventory.Items.Clear();
            foreach (Item item in inventory)
            {
                uiAddToInventory(item);
            }
        }

        private void frmInventory_Load(object sender, EventArgs e)
        {
            lstInventory.Columns.Add("", 0);
            lstInventory.Columns.Add("Code", 60);
            lstInventory.Columns.Add("Name", 120);
            lstInventory.Columns.Add("Price", 80);
            lstInventory.Columns.Add("Quantity", 80);
            lstInventory.Columns.Add("Reorder Limit", 80);

            cboRemoveList.SelectedIndex = 0;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lstInventory.SelectedIndices.Count < 0)
            {
                MessageBox.Show("Select item first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Item item = (Item)lstInventory.SelectedItems[0].Tag;
            frmItem form = new frmItem();
            form.Tag = item;
            DialogResult res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                item = (Item)form.Tag;
                //inventory.Update(item);
                uiUpdateInventory();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string selection = cboRemoveList.SelectedItem.ToString();
            string value = txtRemove.Text;

            if (value == "")
            {
                MessageBox.Show("No value specified", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            bool result = false;
            if (selection == "Code")
            {
                try
                {
                    result = inventory.Remove(Convert.ToInt32(value));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid code value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else if (selection == "Name")
            {
                result = inventory.Remove(value);
            }
            if (result)
            {
                uiUpdateInventory();
                txtRemove.Text = "";
            }
            else
            {
                MessageBox.Show("Not found", "Information");
            }
        }

        private void chkReorderOnly_CheckedChanged(object sender, EventArgs e)
        {
            uiUpdateInventory();
        }
    }
}
