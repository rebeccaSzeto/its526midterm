﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    class Inventory : IEnumerable
    {

        private List<Item> items;

        public Inventory()
        {
            this.items = new List<Item>();
        }

        public void Add(Item item)
        {
            this.items.Add(item);
        }

        public Item Find(int code)
        {
            return this.items.Find(x => x.Code.Equals(code));
        }

        public Item Find(string name)
        {
            return this.items.Find(x => x.Name.Equals(name));
        }

        public bool Remove(int code)
        {
            Item item = this.Find(code);
            if (item != null)
            {
                return this.items.Remove(item);
            }
            else
            {
                return false;
            }
        }

        public bool Remove(string name)
        {
            Item item = this.Find(name);
            if (item != null)
            {
                return this.items.Remove(item);
            }
            else
            {
                return false;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

    }
}
