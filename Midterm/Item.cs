﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    class Item
    {
        private static int counter = 1;
        private const decimal DEFAULT_PRICE = 100.0m;
        private const int DEFAULT_QUANTITY = 1;
        private const int DEFAULT_LIMIT = 1;

        private int code;
        private string name;
        private decimal price;
        private int quantity;
        private int limit;

        public int Code
        {
            get
            {
                return this.code;
            }
            private set
            {
                this.code = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = (value > 0) ? value : DEFAULT_PRICE;
            }
        }

        public int Quantity
        {
            get
            {
                return this.quantity;
            }
            set
            {
                this.quantity = (value > 0) ? value : DEFAULT_QUANTITY;
            }
        }

        public int Limit
        {
            get
            {
                return this.limit;
            }
            set
            {
                this.limit = (value > 0) ? value : DEFAULT_LIMIT;
            }
        }

        public Item()
        {
            this.Code = counter++;
        }

        public Item(string name, decimal price, int quantity, int limit)
        {
            this.Code = counter++;
            this.Name = name;
            this.Price = price;
            this.Quantity = quantity;
            this.Limit = limit;
        }
    }
}
